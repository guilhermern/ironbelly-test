using UnityEngine;
using Project.Common.EventManager;
using Project.Common.GameServices;

namespace Project.Managers
{
    public sealed class GameManager : MonoBehaviour
    {
        private void Awake()
        {
            EventManager eventManager = new EventManager();
            GameServices.RegisterService<IEventManager>(eventManager);
        }
    }
}
