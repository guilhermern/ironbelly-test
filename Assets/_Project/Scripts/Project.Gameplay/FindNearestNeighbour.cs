using UnityEngine;
using Project.Common.ObjectPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Vector3 = UnityEngine.Vector3;

namespace Project.Gameplay
{
    public sealed class FindNearestNeighbour : MonoBehaviour, IPoolObject
    {
        public Transform Transform => transform;

        private ISpawnedCubesListProvider _spawnedCubesListProvider;
        private LineRenderer _lineRenderer;

        public void Setup(ISpawnedCubesListProvider cubesListProvider)
        {
            _spawnedCubesListProvider = cubesListProvider;
            _lineRenderer = GetComponent<LineRenderer>();
        }

        private void Update()
        {
            Vector3 targetPosition = GetNearestCubePoint();

            DrawLine(targetPosition);
        }

        private void DrawLine(Vector3 targetPosition)
        {
            if (targetPosition == Vector3.zero)
            {
                return;
            }

            Debug.DrawLine(transform.position, targetPosition, Color.red);

            _lineRenderer.SetPosition(0, transform.position);
            _lineRenderer.SetPosition(1, targetPosition);
        }

        private Vector3 GetNearestCubePoint()
        {
            Cube cube = _spawnedCubesListProvider.SpawnedCubes.FindClosest(transform.position);

            if (cube == null || cube.Transform == null)
            {
                return Vector3.zero;
            }

            return cube.Transform.position;
        }
    }
}
