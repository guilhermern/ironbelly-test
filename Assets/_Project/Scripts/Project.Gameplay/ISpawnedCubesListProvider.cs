using Project.Common.KdTree;

namespace Project.Gameplay
{
    public interface ISpawnedCubesListProvider
    {
        public KdTree<Cube> SpawnedCubes { get; }
    }
}
