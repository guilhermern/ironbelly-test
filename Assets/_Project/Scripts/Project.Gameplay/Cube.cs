using Project.Common.ObjectPool;
using UnityEngine;

namespace Project.Gameplay
{
    public sealed class Cube : MonoBehaviour, IPoolObject
    {
        [SerializeField] private FindNearestNeighbour _findNearestNeighbour;
        [SerializeField] private RandomMovement _randomMovement;

        public Transform Transform => transform;

        public void Initialize(ISpawnedCubesListProvider cubesListProvider, Vector3 worldBounds)
        {
            _findNearestNeighbour.Setup(cubesListProvider);
            _randomMovement.Setup(worldBounds);
        }
    }
}
