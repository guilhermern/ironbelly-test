using Project.Common.EventManager;
using Project.Common.GameServices;
using Project.Common.KdTree;
using Project.Common.ObjectPool;
using Project.Gameplay.Events;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Project.Gameplay
{
    public sealed class ObjectSpawner : MonoBehaviour, ISpawnedCubesListProvider
    {
        [SerializeField] private Cube _prefab;
        [SerializeField] private Vector3 _spawnZone;
        [SerializeField] private int _startSpawnCount;

        private ObjectPool<Cube> _cubesPool;
        private readonly KdTree<Cube> _spawnedCubes = new KdTree<Cube>();

        private IEventManager _eventManager;

        public KdTree<Cube> SpawnedCubes => _spawnedCubes;

        private void Start()
        {
            _cubesPool = new ObjectPool<Cube>(_prefab, 500, 100);

            _eventManager = GameServices.GetService<IEventManager>();

            _eventManager.AddEventListener<SpawnObjectsEvent>(HandleSpawnObjectsEvent);
            _eventManager.AddEventListener<DespawnObjectsEvent>(HandleDespawnObjectsEvent);

            SpawnCubes(_startSpawnCount);
        }

        private void HandleSpawnObjectsEvent(SpawnObjectsEvent @event)
        {
            SpawnCubes(@event.Quantity);
        }

        private void HandleDespawnObjectsEvent(DespawnObjectsEvent @event)
        {
            DespawnCubes(@event.Quantity);
        }

        private void SpawnCubes(int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                SpawnCube();
            }
        }

        private void DespawnCubes(int quantity)
        {
            quantity = Mathf.Min(quantity, _spawnedCubes.Count);

            for (int i = 0; i < quantity; i++)
            {
                DespawnCube();
            }
        }

        private void SpawnCube()
        {
            float x = Random.Range(0, _spawnZone.x);
            float y = Random.Range(0, _spawnZone.y);
            float z = Random.Range(0, _spawnZone.z);

            Cube spawnedObject = _cubesPool.Spawn(new Vector3(x, y, z)) as Cube;

            if (spawnedObject == null)
            {
                return;
            }

            spawnedObject.Initialize(this, _spawnZone);
            _spawnedCubes.Add(spawnedObject);

            _eventManager.DispatchEvent(new SpawnedObjectsListChangedEvent(_spawnedCubes.Count()));
        }

        private void DespawnCube()
        {
            Cube cube = _spawnedCubes[0];
            _spawnedCubes.RemoveAt(0);

            _cubesPool.Deactivate(cube);

            _eventManager.DispatchEvent(new SpawnedObjectsListChangedEvent(_spawnedCubes.Count()));
        }
    }
}
