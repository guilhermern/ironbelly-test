using UnityEngine;
using Random = UnityEngine.Random;

namespace Project.Gameplay
{
    public sealed class RandomMovement : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private Vector3 _zoneBounds;

        private Vector3 _worldBounds;
        private Vector3 _targetPoint;

        private bool _enabled;

        public void Setup(Vector3 worldBounds)
        {
            _worldBounds = worldBounds;
            GetRandomPoint();

            _enabled = true;
        }


        private void Update()
        {
            Move();
        }

        private void Move()
        {
            if (!_enabled)
            {
                return;
            }

            float step = _speed * Time.deltaTime;

            Vector3 newPosition = Vector3.MoveTowards(transform.position, _targetPoint, step);

            transform.position = newPosition;

            if (Vector3.Distance(transform.position, _targetPoint) < 0.001f)
            {
                GetRandomPoint();
            }
        }

        private void GetRandomPoint()
        {
            Vector3 currentPosition = transform.position;

            float x = Random.Range(currentPosition.x - _zoneBounds.x, currentPosition.x + _zoneBounds.x);
            float y = Random.Range(currentPosition.y - _zoneBounds.y, currentPosition.y + _zoneBounds.y);
            float z = Random.Range(currentPosition.z - _zoneBounds.z, currentPosition.z + _zoneBounds.z);

            x = Mathf.Clamp(x, 0, _worldBounds.x);
            y = Mathf.Clamp(y, 0, _worldBounds.y);
            z = Mathf.Clamp(z, 0, _worldBounds.z);

            _targetPoint = new Vector3(x, y, z);
        }
    }
}
