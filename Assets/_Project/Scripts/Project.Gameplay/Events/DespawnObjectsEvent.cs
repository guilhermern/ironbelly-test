using Project.Common.EventManager;

namespace Project.Gameplay.Events
{
    public sealed class DespawnObjectsEvent : CustomEvent
    {
        public DespawnObjectsEvent(int quantity)
        {
            Quantity = quantity;
        }

        public int Quantity { get; }
    }
}
