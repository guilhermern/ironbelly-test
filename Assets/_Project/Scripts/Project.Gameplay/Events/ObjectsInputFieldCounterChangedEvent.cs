using Project.Common.EventManager;

namespace Project.Gameplay.Events
{
    public sealed class ObjectsInputFieldCounterChangedEvent : CustomEvent
    {
        public ObjectsInputFieldCounterChangedEvent(int newValue)
        {
            CurrentValue = newValue;
        }

        public int CurrentValue { get; }
    }
}
