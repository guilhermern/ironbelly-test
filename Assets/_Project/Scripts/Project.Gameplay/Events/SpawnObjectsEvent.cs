using Project.Common.EventManager;

namespace Project.Gameplay.Events
{
    public sealed class SpawnObjectsEvent : CustomEvent
    {
        public SpawnObjectsEvent(int quantity)
        {
            Quantity = quantity;
        }

        public int Quantity { get; }
    }
}
