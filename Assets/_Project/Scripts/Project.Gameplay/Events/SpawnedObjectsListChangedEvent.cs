using Project.Common.EventManager;

namespace Project.Gameplay.Events
{
    public sealed class SpawnedObjectsListChangedEvent : CustomEvent
    {
        public SpawnedObjectsListChangedEvent(int newSize)
        {
            ListCount = newSize;
        }

        public int ListCount { get; }
    }
}
