using Project.Common.EventManager;
using Project.Common.GameServices;
using Project.Gameplay.Events;
using UnityEngine;
using UnityEngine.UI;

namespace Project.UI
{
    [RequireComponent(typeof(Button))]
    public sealed class DespawnObjectsButton : MonoBehaviour
    {
        private Button _button;
        private IEventManager _eventManager;

        private int _currentSpawnCount;

        private void Start()
        {
            _eventManager = GameServices.GetService<IEventManager>();
            _button = GetComponent<Button>();

            _eventManager.AddEventListener<ObjectsInputFieldCounterChangedEvent>(HandleObjectsInputFieldChanged);

            _button.onClick.AddListener(HandleButtonClicked);
        }

        private void HandleObjectsInputFieldChanged(ObjectsInputFieldCounterChangedEvent @event)
        {
            _currentSpawnCount = @event.CurrentValue;
        }

        private void HandleButtonClicked()
        {
            _eventManager.DispatchEvent(new DespawnObjectsEvent(_currentSpawnCount));
        }
    }
}
