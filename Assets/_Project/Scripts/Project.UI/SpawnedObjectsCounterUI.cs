using Project.Common.EventManager;
using Project.Common.GameServices;
using Project.Gameplay.Events;
using TMPro;
using UnityEngine;

namespace Project.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public sealed class SpawnedObjectsCounterUI : MonoBehaviour
    {
        private TMP_Text _countText;

        private const string DefaultCountText = "Objects count: ";

        private void Start()
        {
            IEventManager eventManager = GameServices.GetService<IEventManager>();
            eventManager.AddEventListener<SpawnedObjectsListChangedEvent>(HandleObjectsCountChanged);

            _countText = GetComponent<TMP_Text>();
        }

        private void HandleObjectsCountChanged(SpawnedObjectsListChangedEvent @event)
        {
            _countText.text = $"{DefaultCountText}{@event.ListCount}";
        }
    }
}
