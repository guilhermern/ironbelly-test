using Project.Common.EventManager;
using Project.Common.GameServices;
using Project.Gameplay.Events;
using System;
using TMPro;
using UnityEngine;

namespace Project.UI
{
    [RequireComponent(typeof(TMP_InputField))]
    public sealed class ObjectSpawnerInputFieldUI : MonoBehaviour
    {
        private TMP_InputField _inputField;
        private IEventManager _eventManager;

        private void Start()
        {
            _eventManager = GameServices.GetService<IEventManager>();

            _inputField = GetComponent<TMP_InputField>();

            _inputField.onEndEdit.AddListener(HandleInputValueChanged);
        }

        private void HandleInputValueChanged(string newValue)
        {
            if (string.IsNullOrEmpty(newValue))
            {
                return;
            }

            if (!int.TryParse(newValue, out int value))
            {
                return;
            }

            _eventManager.DispatchEvent(new ObjectsInputFieldCounterChangedEvent(value));
        }
    }
}
