using System.Collections.Generic;

namespace Project.Common.GameServices
{
    public static class GameServices
    {
        private static readonly Dictionary<int, object> ServiceMap;

        static GameServices()
        {
            ServiceMap = new Dictionary<int, object>();
        }

        public static void RegisterService<T>(T service) where T : class
        {
            ServiceMap[typeof(T).GetHashCode()] = service;
        }

        public static void DeregisterService<T>() where T : class
        {
            ServiceMap.Remove(typeof(T).GetHashCode());
        }

        public static T GetService<T>() where T : class
        {
            object service;
            ServiceMap.TryGetValue(typeof(T).GetHashCode(), out service);
            return (T) service;
        }

        public static void Clear()
        {
            ServiceMap.Clear();
        }
    }
}
