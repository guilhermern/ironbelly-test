using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Common.KdTree
{
    public class KdTree<T> : IEnumerable<T> where T : Component
    {
        private KdNode _root;
        private KdNode _last;
        private KdNode[] _open;
        private int _count;

        public int Count => _count;
        private float AverageSearchLength { set; get; }
        private float AverageSearchDeep { set; get; }

        public T this[int key]
        {
            get
            {
                if (key >= _count)
                {
                    throw new ArgumentOutOfRangeException();
                }

                KdNode current = _root;

                for(int i = 0; i < key; i++)
                {
                    current = current.Next;
                }

                return current.Component;
            }
        }

        public void Add(T item)
        {
            Add(new KdNode() { Component = item });
        }

        public void RemoveAt(int i)
        {
            List<KdNode> list = new List<KdNode>(GetNodes());

            list.RemoveAt(i);
            Clear();

            foreach (KdNode node in list)
            {
                node.OldRef = null;
                node.Next = null;
            }

            foreach (KdNode node in list)
            {
                Add(node);
            }
        }

        public void Clear()
        {
            _root = null;
            _last = null;
            _count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            KdNode current = _root;
            while (current != null)
            {
                yield return current.Component;
                current = current.Next;
            }
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private float Distance(Vector3 a, Vector3 b)
        {
            return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z);
        }

        private float GetSplitValue(int level, Vector3 position)
        {
            return (level % 3 == 0) ? position.x : (level % 3 == 1) ? position.y : position.z;
        }

        private void Add(KdNode newNode)
        {
            _count++;
            newNode.Left = null;
            newNode.Right = null;
            newNode.Level = 0;
            KdNode parent = FindParent(newNode.Component.transform.position);

            if (_last != null)
            {
                _last.Next = newNode;
            }

            _last = newNode;

            if (parent == null)
            {
                _root = newNode;
                return;
            }

            float splitParent = GetSplitValue(parent);
            float splitNew = GetSplitValue(parent.Level, newNode.Component.transform.position);

            newNode.Level = parent.Level + 1;

            if (splitNew < splitParent)
            {
                parent.Left = newNode;
            }
            else
            {
                parent.Right = newNode;
            }
        }

        private KdNode FindParent(Vector3 position)
        {
            KdNode current = _root;
            KdNode parent = _root;

            while (current != null)
            {
                float splitCurrent = GetSplitValue(current);
                float splitSearch = GetSplitValue(current.Level, position);

                parent = current;
                current = splitSearch < splitCurrent ? current.Left : current.Right;
            }

            return parent;
        }

        public T FindClosest(Vector3 position)
        {
            return FindCLose(position);
        }

        private T FindCLose(Vector3 position, ICollection<T> traversed = null)
        {
            if (_root == null)
            {
                return null;
            }

            float nearestDist = float.MaxValue;
            KdNode nearest = null;

            if (_open == null || _open.Length < Count)
            {
                _open = new KdNode[Count];
            }

            for (int i = 0; i < _open.Length; i++)
            {
                _open[i] = null;
            }

            int openAdd = 0;
            int openCur = 0;

            if (_root != null)
            {
                _open[openAdd++] = _root;
            }

            while (openCur < _open.Length && _open[openCur] != null)
            {
                KdNode current = _open[openCur++];
                traversed?.Add(current.Component);

                float nodeDist = Distance(position, current.Component.transform.position);
                if (nodeDist < nearestDist && nodeDist > 0.1f)
                {
                    nearestDist = nodeDist;
                    nearest = current;
                }

                float splitCurrent = GetSplitValue(current);
                float splitSearch = GetSplitValue(current.Level, position);

                if (splitSearch < splitCurrent)
                {
                    if (current.Left != null)
                    {
                        _open[openAdd++] = current.Left;
                    }

                    if (Mathf.Abs(splitCurrent - splitSearch) * Mathf.Abs(splitCurrent - splitSearch) < nearestDist && current.Right != null)
                    {
                        _open[openAdd++] = current.Right;
                    }
                }
                else
                {
                    if (current.Right != null)
                    {
                        _open[openAdd++] = current.Right;
                    }

                    if (Mathf.Abs(splitCurrent - splitSearch) * Mathf.Abs(splitCurrent - splitSearch) < nearestDist && current.Left != null)
                    {
                        _open[openAdd++] = current.Left;
                    }
                }
            }

            AverageSearchLength = (99f * AverageSearchLength + openCur) / 100f;

            if (nearest == null)
            {
                return null;
            }

            AverageSearchDeep = (99f * AverageSearchDeep + nearest.Level) / 100f;

            return nearest.Component;
        }

        private float GetSplitValue(KdNode node)
        {
            return GetSplitValue(node.Level, node.Component.transform.position);
        }

        private IEnumerable<KdNode> GetNodes()
        {
            KdNode current = _root;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

        private class KdNode
        {
            internal T Component;
            internal int Level;
            internal KdNode Left;
            internal KdNode Right;
            internal KdNode Next;
            internal KdNode OldRef;
        }
    }
}
