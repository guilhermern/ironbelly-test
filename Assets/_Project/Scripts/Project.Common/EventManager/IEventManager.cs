﻿namespace Project.Common.EventManager
{
    public interface IEventManager
    {
        void AddEventListener<T>(EventManager.EventDelegate<T> listener) where T : CustomEvent;
        void RemoveEventListener<T>(EventManager.EventDelegate<T> listener) where T : CustomEvent;
        void DispatchEvent(CustomEvent dispatchEvent);
        void DispatchEvent<T>() where T : CustomEvent;
        bool HasListeners<T>();
        void Clear();
    }
}
