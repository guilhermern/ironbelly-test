﻿using System;
using System.Collections.Generic;

namespace Project.Common.EventManager
{
    public abstract class CustomEvent
    {
    }

    public sealed class EventManager : IEventManager
    {
        private struct DelegatePair
        {
            public Type Type;
            public EventDelegate Delegate;
        }

        public delegate void EventDelegate<in T> (T e) where T : CustomEvent;
        private delegate void EventDelegate (CustomEvent e);

        private readonly Dictionary<Type, List<EventDelegate>> _delegates;
        private readonly Dictionary<Delegate, List<DelegatePair>> _delegateCache;
        private readonly Dictionary<Type, bool> _delegatesLock;

        public EventManager()
        {
            _delegates = new Dictionary<Type, List<EventDelegate>>();
            _delegateCache = new Dictionary<Delegate, List<DelegatePair>>();
            _delegatesLock = new Dictionary<Type, bool>();
        }

        ~EventManager()
        {
            Clear();
        }

        public void AddEventListener<T>(EventDelegate<T> del) where T : CustomEvent
        {
            if (!_delegateCache.ContainsKey(del))
            {
                _delegateCache[del] = new List<DelegatePair>();
            }

            Type delegateType = typeof(T);
            if (!_delegates.ContainsKey(delegateType))
            {
                _delegates[delegateType] = new List<EventDelegate>();
            }

            if (TryGetInternalDelegate(delegateType, del, out EventDelegate internalDelegate))
            {
                return;
            }

            internalDelegate = e => del((T)e);
            DelegatePair delegatePair = new DelegatePair
            {
                Type = delegateType,
                Delegate = internalDelegate
            };
            _delegateCache[del].Add(delegatePair);
            _delegates[delegateType].Add(internalDelegate);
        }

        public void RemoveEventListener<T>(EventDelegate<T> del) where T : CustomEvent
        {
            Type delegateType = typeof(T);
            if (!TryGetInternalDelegate(delegateType, del, out EventDelegate internalDelegate))
            {
                return;
            }

            if (!_delegates.ContainsKey(delegateType))
            {
                return;
            }

            int i = _delegates[delegateType].IndexOf(internalDelegate);
            if (i >= 0)
            {
                _delegates[delegateType][i] = null;
            }
            RemoveInternalDelegate(delegateType, del);

            RemoveAllNull(delegateType);
        }

        public void DispatchEvent(CustomEvent dispatchEvent)
        {
            Type type = dispatchEvent.GetType();
            Dispatch(dispatchEvent, type);
        }

        public void DispatchEvent<T>() where T : CustomEvent
        {
            Type type = typeof(T);
            Dispatch(null, type);
        }

        public bool HasListeners<T>()
        {
            Type type = typeof(T);
            return _delegates.ContainsKey(type) && _delegates[type].Count > 0;
        }

        public void Clear()
        {
            _delegates.Clear();
            _delegateCache.Clear();
        }

        private void Dispatch(CustomEvent dispatchEvent, Type type)
        {
            if (!_delegates.ContainsKey(type))
            {
                return;
            }

            _delegatesLock[type] = true;

            for (int i = 0; i < _delegates[type].Count; i++)
            {
                EventDelegate eventDelegate = _delegates[type][i];
                eventDelegate?.Invoke(dispatchEvent);
            }

            _delegatesLock[type] = false;

            RemoveAllNull(type);
        }

        private void RemoveInternalDelegate(Type type, Delegate del)
        {
            if (!_delegateCache.ContainsKey(del))
            {
                return;
            }

            bool found = false;
            for (int i = 0; i < _delegateCache[del].Count && !found; i++)
            {
                DelegatePair pair = _delegateCache[del][i];
                if (pair.Type != type)
                {
                    continue;
                }

                _delegateCache[del].RemoveAt(i);
                found = true;
            }

            if (found && _delegateCache[del].Count == 0)
            {
                _delegateCache.Remove(del);
            }
        }

        private bool TryGetInternalDelegate(Type type, Delegate del, out EventDelegate internalDelegate)
        {
            internalDelegate = null;
            if (!_delegateCache.ContainsKey(del))
            {
                return false;
            }

            for (int i = 0; i < _delegateCache[del].Count; i++)
            {
                DelegatePair pair = _delegateCache[del][i];
                if (pair.Type != type)
                {
                    continue;
                }

                internalDelegate = pair.Delegate;
                return true;
            }

            return false;
        }

        private bool IsDelegateListLocked(Type type)
        {
            return _delegatesLock.ContainsKey(type) && _delegatesLock[type];
        }

        private void RemoveAllNull(Type type)
        {
            if (IsDelegateListLocked(type))
            {
                return;
            }

            if (!_delegates.ContainsKey(type))
            {
                return;
            }

            _delegates[type].RemoveAll(IsNull);

            if (_delegates[type].Count == 0)
            {
                _delegates.Remove(type);
            }
        }

        private bool IsNull(object o)
        {
            return o == null;
        }

    }
}
