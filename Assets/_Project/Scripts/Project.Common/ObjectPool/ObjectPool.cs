using System.Collections.Generic;
using UnityEngine;

namespace Project.Common.ObjectPool
{
    public sealed class ObjectPool<T> where T : MonoBehaviour, IPoolObject
    {
        private readonly Queue<IPoolObject> _pool;

        private readonly T _prefab;
        private readonly int _resizeAmount;

        public ObjectPool(T prefab, int initialSize, int resizeAmount)
        {
            _resizeAmount = resizeAmount;
            _prefab = prefab;

            _pool = new Queue<IPoolObject>();

            PopulateAmount(initialSize);
        }

        public IPoolObject Spawn(Vector3 position)
        {
            if (_pool.Count == 0)
            {
                if (_resizeAmount == 0)
                {
                    Debug.LogWarning("Pool is empty and resize amount is 0, spawn is being ignored");
                    return null;
                }

                Expand();
            }

            IPoolObject obj = _pool.Dequeue();
            obj.Transform.position = position;
            obj.Transform.gameObject.SetActive(true);

            return obj;
        }

        public void Deactivate(T obj)
        {
            obj.gameObject.SetActive(false);
            obj.Transform.position = Vector3.zero;

            _pool.Enqueue(obj);
        }

        public void Deactivate(List<T> objects)
        {
            foreach (T obj in objects)
            {
                obj.gameObject.SetActive(false);
                obj.Transform.position = Vector3.zero;

                _pool.Enqueue(obj);
            }
        }

        private void Expand()
        {
            if (_resizeAmount == 0)
            {
                return;
            }

            PopulateAmount(_resizeAmount);
        }

        private void PopulateAmount(int size)
        {
            for (int i = 0; i < size; i++)
            {
                T obj = Object.Instantiate(_prefab, Vector3.zero, Quaternion.identity);
                obj.gameObject.SetActive(false);

                _pool.Enqueue(obj);
            }
        }
    }
}
