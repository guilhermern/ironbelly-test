using UnityEngine;

namespace Project.Common.ObjectPool
{
    public interface IPoolObject
    {
        Transform Transform { get; }
    }
}
